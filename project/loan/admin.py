from django.contrib import admin
from loan.models import Applicant, Loan
# Register your models here.

admin.site.register(Applicant)
admin.site.register(Loan)
