from django import forms
from loan.models import Loan, Applicant

class LoanForm(forms.Form):
    name = forms.CharField(label= 'Nombre')
    last_name = forms.CharField(label='Apellid')
    dni = forms.IntegerField(label='Nº document')
    gender = forms.ChoiceField(choices=Applicant.GENDER_CHOICES)
    email = forms.CharField(label='E-mail')
    amount = forms.DecimalField(label='Monto solicitado')



class EditLoanForm(LoanForm):
    TRUE_FALSE_CHOICES = (
        (True, 'Yes'),
        (False, 'No'),
        (None, 'En Espera')
    )

    approved = forms.ChoiceField(choices=TRUE_FALSE_CHOICES, label="Aprobado",
                                  initial='', widget=forms.Select(), required=True)
