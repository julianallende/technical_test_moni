from decimal import Decimal
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

# Create your models here.

class Applicant(models.Model):
    GENDER_CHOICES = (
        (u'M', u'Masculino'),
        (u'F', u'Femenino'),
        (u'O', u'Otro'),
        (u'N', u'NS/NC'),
    )

    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    dni = models.PositiveIntegerField(validators=[MaxValueValidator(100000000)])
    gender = models.CharField(max_length=2, choices=GENDER_CHOICES)
    email = models.CharField(max_length=100)


class Loan(models.Model):
    applicant = models.ForeignKey('loan.Applicant', on_delete=models.CASCADE)

    amount = models.DecimalField(decimal_places=2,
                                 max_digits=10,
                                 validators=[MinValueValidator(Decimal('0.00'))],
                                 null=True,
                                 blank=True)

    approved = models.NullBooleanField(default=None)
