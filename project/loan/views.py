import json

from decimal import Decimal

from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.core import serializers
from loan.forms import LoanForm, EditLoanForm
from loan.models import Loan, Applicant
from django.http import JsonResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
import requests


def home(request):
    template_name = 'home.html'
    return render(request, template_name=template_name)


def loan_request(request):
    template_name = 'request_loan.html'
    form = LoanForm()
    return render(request, template_name,
                  context={
                      'form': form,
                      'approved': False
                  })

@csrf_exempt
def validate_data(request):
    api_endpoint = 'http://scoringservice.moni.com.ar:7001/api/v1/scoring/'
    approved = False
    error = False
    if request.POST and request.is_ajax():
        form = LoanForm(request.POST)
        if form.is_valid():
            name = request.POST.get('name', None)
            last_name = request.POST.get('last_name', None)
            dni = request.POST.get('dni', None)
            gender = request.POST.get('gender', None)
            email = request.POST.get('email', None)
            amount = request.POST.get('amount', None)
            data = {
                'name': name,
                'last_name': last_name,
                'document_number': dni,
                'gender': gender,
                'email': email,
                'amount': amount
            }
            response = requests.get(api_endpoint, params=data)
            context = response.json()
            approved = context.get('approved', None)
            error = context.get('error', None)
            try:
                applicant = Applicant.objects.create(
                    name=name,
                    last_name=last_name,
                    dni= dni,
                    gender=gender,
                    email=email
                )
                Loan.objects.create(
                    applicant=applicant,
                    amount=Decimal(float(amount)),
                    approved = approved
                )
            except Exception:
                approved = False
                error = True
    data = {
        'approved': approved,
        'error': error
    }
    return JsonResponse(data)


@user_passes_test(lambda u:u.is_staff, login_url=reverse_lazy('auth_login'))
def loan_admin(request):
    template_name = 'loan_list.html'

    loan_list = Loan.objects.all()
    page = request.GET.get('page', 1)
    paginator = Paginator(loan_list, 10)

    try:
        loans = paginator.page(page)
    except PageNotAnInteger:
        loans = paginator.page(1)
    except EmptyPage:
        loans = paginator.page(paginator.num_pages)

    return render(request, template_name=template_name, context={'loan_list':loans})


@user_passes_test(lambda u:u.is_staff, login_url=reverse_lazy('auth_login'))
def loan_edit(request, loan_id):
    template_name = 'edit_loan.html'
    loan = get_object_or_404(Loan,id=loan_id)
    form = EditLoanForm()
    print(request.method)
    if request.method == 'POST':
        print(request.POST.get('approved'))
        loan.approved = request.POST.get('approved')
        loan.amount = request.POST.get('amount')
        applicant = loan.applicant
        applicant.name = request.POST.get('name')
        applicant.last_name = request.POST.get('last_name')
        applicant.dni = request.POST.get('dni')
        applicant.gender = request.POST.get('gender')
        applicant.email = request.POST.get('email')
        applicant.save()
        loan.save()
        return HttpResponseRedirect('/loan_admin')
    elif request.method == 'GET':
        form.initial = {
            'name':loan.applicant.name,
            'last_name':loan.applicant.last_name,
            'dni':loan.applicant.dni,
            'gender':loan.applicant.gender,
            'email':loan.applicant.email,
            'amount':loan.amount,
            'approved': loan.approved
        }
    return render(request, template_name,
                  context={'form': form, 'loan': loan})


@csrf_exempt
@user_passes_test(lambda u:u.is_staff, login_url=reverse_lazy('auth_login'))
def loan_delete(request):
    loan_id = None
    if request.POST and request.is_ajax():
        id = request.POST.get('id')
        try:
            loan = Loan.objects.get(id = id)
            loan_id = loan.id
            loan.delete()
        except Exception:
            loan_id = None
    data = {'id': loan_id}
    return JsonResponse(data)

