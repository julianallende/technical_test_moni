from django.urls import path
from django.conf.urls import url

from loan import views

urlpatterns = [
    path(r'',views.home, name='home'),
    path(r'loan_request', views.loan_request, name='loan_request'),
    path(r'loan_admin', views.loan_admin, name='loan_admin'),
    # path(r'/loan_edit/(?P<loan_id>\d+)/', views.loan_edit, name='loan_edit'),
    # url(r'^accounts/login/$', auth_views.login, name='auth_login'),
    url(r'^loan_edit/(?P<loan_id>\d+)/$', views.loan_edit, name='loan_edit'),
    url(r'^ajax/validate_data/$', views.validate_data, name='validate_data'),
    url(r'^ajax/loan_delete/$', views.loan_delete, name='loan_delete'),
]


